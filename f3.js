// passing values to a function
function hello(name) {
    return "Function says hello " + name
}

document.getElementById("hemi").innerHTML = (hello("Hemi"))

//creating and using a variable for the function
let personName = "Jake"
document.getElementById("jake").innerHTML = (hello(personName))

